var docClient = require('../api');
var helpers     = require('../utils/helpers');
var moment = require('moment');

// const table = "InvoiceRows";


const rootController = (req, res, next) => {

  if (req.query.year){
    var params = {
      TableName: "InvoiceRows"
    };
  }
  else{
    const oneYearAgo = moment().subtract(1, 'year').format("YYYY-MM-DD").toString();
    var params = {
      TableName: "InvoiceRows",
      FilterExpression: "#invoiceDate > :from",
      ExpressionAttributeNames: {
        "#invoiceDate": "invoiceDate",
      },
      ExpressionAttributeValues: {
        ":from": { "S": `${oneYearAgo}` }
      }
    };
  }
  
    docClient.scan(params, function(err, result) {
      if(err) {
        console.log(err);
        res.render('error');
      }else if (result && !err){
        res.render('invoices', {invoices: result.Items});
      }
    })
  
}

const invoiceController = (req, res, next) => {
  let date = req.params.date;
  let id = req.params.id;
  let invoiceId = date + '/' + id;

  var invoiceRows = [];
  
  var paramsOneInvoiceRowId = {
    KeyConditionExpression: '#id = :v1',
    ExpressionAttributeValues: {
      ':v1': { S: invoiceId}
    },
    ExpressionAttributeNames: {
            "#id":"invoiceId"
    },
    TableName: "InvoiceRows"
  }

  docClient.query(paramsOneInvoiceRowId, function(err, data) {

    if (err){
      res.render('error');
    }
    else {
      res.render('invoice', {invoiceRows: data.Items});
    }
  })

}


module.exports = {
  rootController,
  invoiceController
}



