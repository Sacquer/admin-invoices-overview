var qs        = require('qs');
var docClient = require('../api');
var nanoid    = require('nanoid');


const rootController = (req, res, next) => {
  res.render('new_customer', { customer: { id: nanoid(), url: '/contract-agreement/' } } );
}

const createCustomerController = (req, res, next) => {

  const {
    customerName,
    customerContactPerson,
    customerPhoneNumber,
    customerId,
    customerUrl,

    subscriptionPrefixWebsite,
    subscriptionPackage,
    subscriptionCashRegister,

    payementPlaceCompanyName,
    payementPlaceOrganisationNumber,
    payementPlaceStreetAddress,
    payementPlaceZipCode,
    payementPlaceCity,
    payementPlacePhoneNumber,
    payementPlaceFaxNumber,
    payementPlaceEmail,
    payementPlaceWebsite,
    payementPlaceYearCount,
    payementPlaceDate,

    bankBgNumber,
    bankBgReceiver,
    bankTaxNumber,
    bankSwiftAddress,
    bankIbanNumber,
    settingsPrivateCustomer,
    settingsCompanyCustomer,
    settingsAgentNumber,
    settingsInvoiceMessage,
    settingsInvoiceStartDate,

    users

  } = req.body;

  
  var params = {
    Item: {
      "name": {
        S: customerName
      },
      "contactPerson": {
        S: customerContactPerson
      },
      "customerPhoneNumber": {
        S: customerPhoneNumber
      },
      "customerId": {
        S: customerId
      },
      "ownerUrl": {
        S: customerUrl + customerId
      },
      "date": {
        S: new Date().toString()
      },
      "websitePrefix": {
        S: subscriptionPrefixWebsite
      },
      "package": {
        S: subscriptionPackage
      },
      "cashRegisters":{
        S: subscriptionCashRegister
      },
      "companyName":{
        S: payementPlaceCompanyName
      },
      "orgNumber":{
        S: payementPlaceOrganisationNumber
      },
      "streetAddress":{
        S: payementPlaceStreetAddress
      },
      "zip":{
        S: payementPlaceZipCode
      },
      "city":{
        S: payementPlaceCity
      },
      "phoneNumber":{
        S: payementPlacePhoneNumber
      },
      "faxNumber":{
        S: payementPlaceFaxNumber
      },
      "email":{
        S: payementPlaceEmail
      },
      "website":{
        S: payementPlaceWebsite
      },
      "yearCount":{
        S: payementPlaceYearCount
      },
      "accountingDate":{
        S: payementPlaceDate
      },
      "bgNumber": {
        S: bankBgNumber
      },
      "bgReceiver": {
        S: bankBgReceiver
      },
      "taxRegNumber": {
        S: bankTaxNumber
      },
      "swiftAddress": {
        S: bankSwiftAddress
      },
      "iban": {
        S: bankIbanNumber
      },
      "settingsPrivateDays": {
        S: settingsPrivateCustomer
      },
      "settingsCompanyDays": {
        S: settingsCompanyCustomer
      },
      "agentNumber": {
        S: settingsAgentNumber
      },
      "invoiceMsg": {
        S: settingsInvoiceMessage
      },
      "invoiceStartDate": {
        S: settingsInvoiceStartDate
      },
      "users": {
        L: users
      }
    },
    TableName: "CustomersPending"
  };

  docClient.putItem(params, function(err, data) {
    if (err) console.log(err, err.stack);
    else console.log('SUCCESS: ', data);
  });

  console.log(req.body);
    res.send({ data: req.body });
};

module.exports = {
  rootController,
  createCustomerController,
}