var docClient = require('../api');


const rootController = (req, res, next) => {
  res.render('contractAgreement' );
  
}

const contractAgreementController = (req, res, next) => {

  var params = {
    ExpressionAttributeValues: {
      ":v1": {
        S: req.params.id
      }
    },
    KeyConditionExpression: "customerId = :v1",
    TableName: "CustomersPending"
  }

  docClient.query(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else {
      console.log(JSON.stringify(data, undefined, 2));
      res.render('contractAgreement' );
    }
  })
}

module.exports = {
  rootController,
  contractAgreementController,
}