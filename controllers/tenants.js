var docClient   = require('../api');
var helpers     = require('../utils/helpers');
var moment = require('moment');

const table = "Customers";
const params = { TableName: table };

const paramsInvoice = {TableName: "InvoiceRows"};


const rootController = (req, res, next) => {
  docClient.scan(params, function(err, result) {
  })
}

const tenantController = (req, res, next) => {
  //Empty object to fill result from db
  var customer = {};

  var tenant = tenants.filter(tenant => tenant['customer-id'].S == req.params.id);

  var nextTenantIndex = tenant[0].index + 1;
  console.log('NexttenantIndex: ', nextTenantIndex);

  var nextTenant = tenants.filter(tenant => tenant.index == nextTenantIndex);
  console.log('NextTenant: ', nextTenant);

  //Preparing question param to db, to get one customer
  var paramsOneCustomer = {
      TableName: "Customers",
      KeyConditionExpression: '#id = :v2',
      ExpressionAttributeNames: {
        "#id": "customer-id",
      },
      ExpressionAttributeValues: {
        ":v2": { "S": req.params.id }
      }
  }

  //Executing question and adding data to empty customer objecr
  docClient.query(paramsOneCustomer, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else {
      customer = data.Items[0];
    }
  })

  //Get all invoiceRows
  docClient.scan(paramsInvoice, function(err, result) {
  //Get invoices from specific customer
    let invoices = helpers.getInvoices(result.Items, customer['customer-id'].S);
  //Index of first select dropdown fro displaying specifc month
    let first = req.query.first;
  //Index of second select dropwdown
    let second = req.query.second;

    var tableOne;
    var tableTwo;

    if (first) {
      //Get invoices for first custom month
      let firstCustomMonth = helpers.getCustomMonth(invoices, first);
      //Get date from selected invoices
      let firstDate = moment(firstCustomMonth[0].invoiceDate.S).locale('sv');
      //Get the month name
      let firstMonth = firstDate.format('MMMM');
      //Add month name to first object in object array for displaying on page
      firstCustomMonth[0].month = firstMonth;
      //Add invoice rows from first custom table to tableOne
      tableOne = firstCustomMonth;
    }
    else{
      //Get last month invoice-rows as default if no custom-value
      let lastMonthInvoices = helpers.getLastMonthInvoices(invoices);
      if (lastMonthInvoices === undefined || lastMonthInvoices.length == 0) {
        
      }
      else{
        //Get date from selected invoices
        let lastDate = moment(lastMonthInvoices[0].invoiceDate.S).locale('sv');
        //Get the month name
        let lastMonth = lastDate.format('MMMM');
        //Add month name to first object in object array for displaying on page
        lastMonthInvoices[0].month = lastMonth;
      }
      //Add invoice rows from first custom table to tableOne
      tableOne = lastMonthInvoices;
    }

    if (second) {
      //Get invoices for second custom month
      let secondCustomMonth = helpers.getCustomMonth(invoices, second);
      //Get date from selected invoices
      let date = moment(secondCustomMonth[0].invoiceDate.S).locale('sv');
      //Get the month name
      let month = date.format('MMMM');
      //Add month name to first object in object array for displaying on page
      secondCustomMonth[0].month = month;
      //Add invoice rows from first custom table to tableTwo
      tableTwo = secondCustomMonth;
    }
    else{
      //Get two months ago invoice-rows as default if no custom-value for second select
      let lastTwoMonthsAgo = helpers.getTwoMonthsAgo(invoices);
      if (lastTwoMonthsAgo === undefined || lastTwoMonthsAgo.length == 0) {
        
      }
      else{
        //Get date from selected invoices
        let secondDate = moment(lastTwoMonthsAgo[0].invoiceDate.S).locale('sv');
        //Get the month name
        let secondMonth = secondDate.format('MMMM');
        //Add month name to first object in object array for displaying on page
        lastTwoMonthsAgo[0].month = secondMonth;
      }

      //Add invoice rows from first custom table to tableTwo
      tableTwo = lastTwoMonthsAgo;
    }
    //Contruct object to the view. 
    let tenant = {customer: customer, invoices: invoices, tableOne: tableOne, tableTwo: tableTwo}
    res.render('tenant', {tenant: {...tenant}});
  })
}

//Export controllers
module.exports = {
  rootController,
  tenantController
}

      



