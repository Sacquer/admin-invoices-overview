var docClient = require('../api');

const table = "Customers";

const params = { TableName: table };


const rootController = (req, res, next) => {
  docClient.scan(params, function(err, result) {
    tenants = [];
    var tenantsSorted = result.Items.sort(function(a, b){
      if(a.name < b.name) { return -1; }
      if(a.name > b.name) { return 1; }
      return 0;
    });
    for ( let i = 0; i < tenantsSorted.length; i++) {
        result.Items[i].index = i;
        tenants.push(result.Items[i]);
     }
    res.render('tenants', { customers: tenants });
  })
}

const loginController = (req, res, next) => {
  res.render('login', { title: 'Login' });
}



module.exports = {
  rootController,
  loginController
}