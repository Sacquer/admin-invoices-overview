var moment = require('moment');

function getInvoices (data, id) {
  const resultInvoices = data.filter(obj => {
    if (obj.invoiceId){
      return obj.invoiceId.S.includes(id);
    }
  });
  return resultInvoices;
}


function sortInvoices (data, sort) {
  
  if (sort == 'invoiceId'){
    var invoiceId = data.sort(function(a, b) { 
      return a.invoiceId.S == b.invoiceId.S;
  })
    return invoiceId;
  }

  if(sort== 'date'){
    let temp = data.sort(function(a, b) {
      a = a.invoiceDate.S.split('-').join('');
      b = b.invoiceDate.S.split('-').join('');
      return b - a; 
    })
    return temp;
  }
}

function sortTenants(data, filter) {
  if (filter == 'date') {
    let temp = data.sort(function(a, b) {
      a = a.accept_date.S.split('-').join('');
      b = b.accept_date.S.split('-').join('');
      return b - a;
    })
    return temp;
  }
  if (filter == 'name') {
    let temp = data.sort(function(a, b) {
      a = a.name.S.toLowerCase().replace(/\s+/g, '');
      b = b.name.S.toLowerCase().replace(/\s+/g, '');
      return a.localeCompare(b);
    })
    return temp;
  }
  if (filter == 'city') {
    let temp = data.sort(function(a, b){
      if (a.city !== undefined && b.city !== undefined){
        a = a.city.S.toLowerCase().replace(/\s+/g, '');
        b = b.city.S.toLowerCase().replace(/\s+/g, '');
        return a.localeCompare(b);
      }
    });
    return temp;
  }
}

function searchTenants(data, search){
  let result = [];

  const resultName = data.filter(obj => {
    if (obj.name){
      return obj.name.S.toLowerCase().includes(search.toLowerCase());
    }
  });
  const resultCity = data.filter(obj => {
    if (obj.city){
      return obj.city.S.toLowerCase().includes(search.toLowerCase());
    }
  });
  const resultDate = data.filter(obj => {
    if (obj.accept_date){
      return obj.accept_date.S.toLowerCase().includes(search.toLowerCase());
    }
  });

  if (resultName.length !== 0) {
    result.push(...resultName);
    // result = [...result, resultName];
  }
  if (resultCity.length !== 0) {
    result.push(...resultCity);
    // result = [...result, resultCity];
  }
  if (resultDate.length !== 0){
    result.push(...resultDate);
    // result = [...result, resultDate];
  }
  return result;
}

function getByDate(date){
  return data.filter(function (el) {
    return el.date == date;
  });
}

function getLastMonthInvoices(data){
  let startOfLastMonth = moment().subtract(1,'month').startOf('month');
  let endOfLastMonth = moment().subtract(1,'month').endOf('month');

  const lastMonthInvoices =  data.filter(function (item) {
    return moment(item.invoiceDate.S).isBetween(startOfLastMonth, endOfLastMonth);
  });

  return lastMonthInvoices;
}

function getTwoMonthsAgo(data){
  let startOfTwoMonths = moment().subtract(2,'months').startOf('month');
  let endOfTwoMonths = moment().subtract(2,'months').endOf('month');

  const twoMonthsAgoInvoices =  data.filter(function (item) {
    return moment(item.invoiceDate.S).isBetween(startOfTwoMonths, endOfTwoMonths);
  });

  return twoMonthsAgoInvoices;
}

function getCustomMonth(data, month){

  let startOfMonth = moment().startOf('month').month(month);
  let endOfMonth = moment().endOf('month').month(month);

  const customFirstMonth =  data.filter(function (item) {
    if (moment(endOfMonth).isBefore(moment())){
      return moment(item.invoiceDate.S).isBetween(startOfMonth, endOfMonth);
    }
    else {
      let startMonthLastYear = moment().subtract(1,'year').month(month).startOf('month');
      let endMonthLastYear = moment().subtract(1,'year').month(month).endOf('month');

      return moment(item.invoiceDate.S).isBetween(startMonthLastYear, endMonthLastYear);
    }
  });

 return customFirstMonth;
}
  
module.exports = {
  sortTenants,
  searchTenants,
  sortInvoices,
  getInvoices,
  getLastMonthInvoices,
  getTwoMonthsAgo,
  getCustomMonth
};

