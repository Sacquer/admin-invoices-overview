$(document).ready(function() {
  // Adjust select values in dropdown-tenant
   selectValues();   

   //Activare all the tables
  $('#tenants-table').DataTable( {
      "paging":   false,
      "info": false
  });

  $('#invoices-table').DataTable( {
    "paging":   false,
    "info": true,
    "order": [[ 1, "desc" ]]
  });

  $('#tenant-table').DataTable( {
    "paging":   false,
    "info": false,
    "order": [[ 1, "desc" ]]
  });

  $('#last-month-table').DataTable( {
    "paging":   false,
    "info": false,
    "search": false,
    "ordering": false,
    "order": [[ 1, "desc" ]]
  });

  $('#two-months-table').DataTable( {
    "paging":   false,
    "info": false,
    "ordering": false,
    "order": [[ 1, "desc" ]]
  });

  $('#three-months-table').DataTable( {
    "paging":   false,
    "info": false,
    "order": [[ 1, "desc" ]]
  });
  
  //Add placeholder do dynamicly created table-search-input
  $('input[type="search"]').attr("placeholder","Sök");

  //Activates bootstrap tooltip, used in template for customer
  $('[data-toggle="tooltip"]').tooltip();   

});

  //Assure to make table tr/td to links
$('*[data-href]').on("click",function(){
  window.location = $(this).data('href');
  return false;
});
 //Assure to make table tr/td to links
$("td > a").on("click",function(e){
  e.stopPropagation();
});

// Function that adjusts select values in dropdown-tenant
function selectValues() {
  //First dropdown
  switch($('#firstMonth').attr("name")) {
    case "januari":
    $('#firstMonth :nth-child(1)').prop('selected', true);
     break;
    case "februari":
    $('#firstMonth :nth-child(2)').prop('selected', true);
     break;
     case "mars":
     $('#firstMonth :nth-child(3)').prop('selected', true);
      break;
      case "april":
    $('#firstMonth :nth-child(4)').prop('selected', true);
     break;
     case "maj":
     $('#firstMonth :nth-child(5)').prop('selected', true);
      break;
      case "juni":
    $('#firstMonth :nth-child(6)').prop('selected', true);
     break;
     case "juli":
     $('#firstMonth :nth-child(7)').prop('selected', true);
      break;
      case "augusti":
    $('#firstMonth :nth-child(8)').prop('selected', true);
     break;
     case "september":
     $('#firstMonth :nth-child(9)').prop('selected', true);
      break;
      case "oktober":
    $('#firstMonth :nth-child(10)').prop('selected', true);
     break;
     case "november":
     $('#firstMonth :nth-child(11)').prop('selected', true);
      break;
      case "december":
    $('#firstMonth :nth-child(12)').prop('selected', true);
     break;
    default:
   }  
   //Second dropwdown
   switch($('#secondMonth').attr("name")) {
    case "januari":
    $('#secondMonth :nth-child(1)').prop('selected', true);
     break;
    case "februari":
    $('#secondMonth :nth-child(2)').prop('selected', true);
     break;
     case "mars":
     $('#secondMonth :nth-child(3)').prop('selected', true);
      break;
      case "april":
    $('#secondMonth :nth-child(4)').prop('selected', true);
     break;
     case "maj":
     $('#secondMonth :nth-child(5)').prop('selected', true);
      break;
      case "juni":
    $('#secondMonth :nth-child(6)').prop('selected', true);
     break;
     case "juli":
     $('#secondMonth :nth-child(7)').prop('selected', true);
      break;
      case "augusti":
    $('#secondMonth :nth-child(8)').prop('selected', true);
     break;
     case "september":
     $('#secondMonth :nth-child(9)').prop('selected', true);
      break;
      case "oktober":
    $('#secondMonth :nth-child(10)').prop('selected', true);
     break;
     case "november":
     $('#secondMonth :nth-child(11)').prop('selected', true);
      break;
      case "december":
    $('#secondMonth :nth-child(12)').prop('selected', true);
     break;
    default:
   }  
}

//Add user input row for contract-agreement 
function addUser (contract) {

  //Check if row is added from us och customer contract
  if (contract){
    var color = "orange";
    var orangeHover = "contract-hover";
  }
  else{
    var color = "blue";
    var orangeHover = "new-hover";
  }
  //Html template for row added
  let htmlRow = 
  `
  <div  class= "user form-container">
    <div class="input-flex-container dynamic" style="margin-top: 75px;">
      <i class="material-icons ${color}" style="position: absolute; left: 30px;">person</i>
      <i class="material-icons ${color} ${orangeHover} icon-remove" onclick="removeUser(this)" style="
        position: absolute;
        float: right;
        right: 30px;
        ">
        remove_circle
      </i>
    <div class="form-group">
      <label for="userWorkTitle">Yrkesroll </label>
      <select class="custom-select" id="userWorkTitle" name="userWorkTitle">
        <option value="veterinary">Veterinär</option>
        <option value="veterinaryCandidate">Veterinärkandidat</option>
        <option value="foreignVeterinary">Utländsk veterinär</option>
        <option value="legitimizedAnimalCaretaker">Legitimerad djurskötare</option>
        <option value="approvedNurse">Godkänd sjuksköterska</option>
        <option value="assistant">Assistent</option>
        <option value="approvedPhysiotherapist">Godkänd sjukgymnast</option>
        <option value="approvedDentist">Godkänd tandläkare</option>
        <option value="approvedFarrier">Godkänd hovslagare</option>
        <option value="administrativeAssistant">Administrativ assistent</option>
        <option value="other">Övrig</option>
      </select>
    </div>
    <div class="form-group">
      <label for="userFullName">Fullständigt namn </label>
      <input class="form-control" id="userFullName" type="text" name="userFullName" placeholder="Förnamn och efternamn" />
    </div>
    <div class="form-group">
      <label for="userVetNumber">id/assitentnummer </label>
      <input class="form-control" id="userVetNumber" type="text" name="userVetNumber" placeholder="Veterinärid eller Assistentnummer" />
    </div>
    </div>
    <div class="input-flex-container">
      <div class="form-group">
        <label for="userSsn">Personnummer </label>
        <input class="form-control" id="userSsn" type="text" name="userSsn" placeholder="Personnummer"/>
      </div>
      <div class="form-group">
        <label for="userEmail">E-post </label>
        <input class="form-control" id="userEmail" type="text" name="userEmail" placeholder="E-post" />
      </div>
      <div class="form-group">
        <label for="userTeleNumber">Telefonnummer </label>
        <input class="form-control" id="userTeleNumber" type="text" name="userTeleNumber" placeholder="Telefonnummer" />
      </div>
    </div>
    <div class="input-flex-container">
      <div class="form-group">
        <label for="userAddress">Adress </label>
        <input class="form-control" id="userAddress" type="text" name="userAddress" placeholder="Adress" />
      </div>
      <div class="form-group">
        <label for="userZipCode">Postnummer </label>
        <input class="form-control" id="userZipCode" type="text" name="userZipCode" placeholder="Postnummer" />
      </div>
      <div class="form-group">
        <label for="userCity">Stad </label>
        <input class="form-control" id="userCity" type="text" name="userCity" placeholder="Telefonnummer" />
      </div>
    </div>
  </div>
  `
  document.getElementById('newRow').insertAdjacentHTML('beforeend', htmlRow);
}

//Function to remove user-row
function removeUser (e) {
  var x =  e.parentElement.parentElement;
  x.setAttribute("class", "display-none");

}

//Function for copy link-text
function copy () {
  const copyText = document.getElementById("link");
  range = document.createRange();
  range.selectNode(copyText);
  window.getSelection().addRange(range);
  document.execCommand("copy");

  const linkLabel = document.getElementById("linkhelp");
  linkLabel.innerHTML = "Kopierad!";
}

//Function to display add-user-row button. Only when multi or business is selected
function displayAddBtn () {
  $('#addBtn').show();
}

//Function to display add-user-row button. Only when multi or business is selected
function hideAddBtn () {
  $('#addBtn').hide();
}

//Construct url parameter to get selected month tables. 
function showMonths (id){
  var el = document.getElementById("firstMonth");
  var firstMonth = el.options[el.selectedIndex].value;

  var e = document.getElementById("secondMonth");
  var secondMonth = e.options[e.selectedIndex].value;

  window.location.href = "/tenants/"+id+"?first="+firstMonth+"&second="+secondMonth;
}


//Post from new_customer 
function submitForm (e){
  e.preventDefault();

  customerName = document.getElementById("customerName").value;
  customerContactPerson = document.getElementById("customerContactPerson").value;
  customerPhoneNumber = document.getElementById("customerPhoneNumber").value;
  customerId = document.getElementById("customerId").value;
  customerUrl = document.getElementById("customerUrl").value;

  subscriptionPrefixWebsite = document.getElementById("subscriptionPrefixWebsite").value;
  subscriptionPackage = $('input[name=subscriptionPackage]:checked').val();
  subscriptionCashRegister = $('input[name=subscriptionCashRegister]:checked').val();

  payementPlaceCompanyName = document.getElementById("payementPlaceCompanyName").value;
  payementPlaceOrganisationNumber = document.getElementById("payementPlaceOrganisationNumber").value;
  payementPlaceStreetAddress = document.getElementById("payementPlaceStreetAddress").value;
  payementPlaceZipCode = document.getElementById("payementPlaceZipCode").value;
  payementPlaceCity = document.getElementById("payementPlaceCity").value;
  payementPlacePhoneNumber = document.getElementById("payementPlacePhoneNumber").value;
  payementPlaceFaxNumber = document.getElementById("payementPlaceFaxNumber").value;
  payementPlaceEmail = document.getElementById("payementPlaceEmail").value;
  payementPlaceWebsite = document.getElementById("payementPlaceWebsite").value;
  payementPlaceYearCount = $('input[name=payementPlaceYearCount]:checked').val();
  payementPlaceDate = document.getElementById("payementPlaceDate").value;

  bankBgNumber = document.getElementById("bankBgNumber").value;
  bankBgReceiver = document.getElementById("bankBgReceiver").value;
  bankTaxNumber = document.getElementById("bankTaxNumber").value;
  bankSwiftAddress = document.getElementById("bankSwiftAddress").value;
  bankIbanNumber = document.getElementById("bankIbanNumber").value;

  settingsPrivateCustomer = document.getElementById("settingsPrivateCustomer").value;
  settingsCompanyCustomer = document.getElementById("settingsCompanyCustomer").value;
  settingsAgentNumber = document.getElementById("settingsAgentNumber").value;
  settingsInvoiceMessage = document.getElementById("settingsInvoiceMessage").value;
  settingsInvoiceStartDate = document.getElementById("settingsInvoiceStartDate").value;


  function User (userWorkTitle, userFullName, userVetNumber, userSsn, userEmail, userTeleNumber, userAddress, userZipCode, userCity) {
    this.workTitle = userWorkTitle;
    this.fullName = userFullName;
    this.vetNumber = userVetNumber;
    this.Ssn = userSsn;
    this.email = userEmail;
    this.teleNumber = userTeleNumber;
    this.address = userAddress;
    this.zipCode = userZipCode;
    this.city = userCity;
  }

  var users = [];

  $(".user ").each(function(){
    var $this = $(this);
    var userWorkTitle = $this.find('#userWorkTitle').val();
    var userFullName = $this.find('input#userFullName').val();
    var userVetNumber = $this.find('input#userVetNumber').val();
    var userSsn = $this.find('input#userSsn').val();
    var userEmail = $this.find('input#userEmail').val();
    var userTelenumber = $this.find('input#userTeleNumber').val();
    var userAddress = $this.find('input#userAddress').val();
    var userZipCode = $this.find('input#userZipCode').val();
    var userCity = $this.find('input#userCity').val();

    var user = new User(userWorkTitle, userFullName, userVetNumber, userSsn, userEmail, userTelenumber, userAddress, userZipCode, userCity);
    users.push(user);
  });


  axios.post('/new/create', {
    customerName: customerName,
    customerContactPerson: customerContactPerson,
    customerPhoneNumber: customerPhoneNumber,
    customerId: customerId,
    customerUrl: customerUrl,

    subscriptionPrefixWebsite: subscriptionPrefixWebsite,
    subscriptionPackage: subscriptionPackage,
    subscriptionCashRegister: subscriptionCashRegister,

    payementPlaceCompanyName: payementPlaceCompanyName,
    payementPlaceOrganisationNumber: payementPlaceOrganisationNumber,
    payementPlaceStreetAddress: payementPlaceStreetAddress,
    payementPlaceZipCode: payementPlaceZipCode,
    payementPlaceCity: payementPlaceCity,
    payementPlacePhoneNumber: payementPlacePhoneNumber,
    payementPlaceFaxNumber: payementPlaceFaxNumber,
    payementPlaceEmail: payementPlaceEmail,
    payementPlaceWebsite: payementPlaceWebsite,
    payementPlaceYearCount: payementPlaceYearCount,
    payementPlaceDate: payementPlaceDate,

    bankBgNumber: bankBgNumber,
    bankBgReceiver: bankBgReceiver,
    bankTaxNumber: bankTaxNumber,
    bankSwiftAddress: bankSwiftAddress,
    bankIbanNumber: bankIbanNumber,

    settingsPrivateCustomer: settingsPrivateCustomer,
    settingsCompanyCustomer: settingsCompanyCustomer,
    settingsAgentNumber: settingsAgentNumber,
    settingsInvoiceMessage: settingsInvoiceMessage,
    settingsInvoiceStartDate: settingsInvoiceStartDate,
    users: users
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
}



