var express           = require('express');
var tenantControllers = require('../controllers/tenants');

var router  = express.Router();

/* GET tenants listing. */
router.get('/', tenantControllers.rootController);

router.get('/:id', tenantControllers.tenantController);

module.exports = router;
