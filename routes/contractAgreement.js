var express                       = require('express');
var contractAgreementControllers  = require('../controllers/contractAgreement');

var router = express.Router();

router.get('/', contractAgreementControllers.rootController);

router.get('/:id', contractAgreementControllers.contractAgreementController);

module.exports = router;