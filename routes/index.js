var express         = require('express');
var rootControllers = require('../controllers/root');

var router = express.Router();

/* GET home page. */
router.get('/', rootControllers.rootController);

router.get('/login', rootControllers.loginController);

module.exports = router;