var express                 = require('express');
var newCustomerControllers  = require('../controllers/new_customer');

var router = express.Router();

router.get('/', newCustomerControllers.rootController);

router.post('/create', newCustomerControllers.createCustomerController);

module.exports = router;
