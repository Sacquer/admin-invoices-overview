var express             = require('express');
var invoiceControllers  = require('../controllers/invoices');

var router = express.Router();

/* GET home page. */
router.get('/', invoiceControllers.rootController);

router.get('/:date/:id', invoiceControllers.invoiceController);

module.exports = router;




